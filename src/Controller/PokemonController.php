<?php

// src/Controller/PokemonController.php
namespace App\Controller;

use App\Service\PokemonApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PokemonController extends AbstractController
{
    protected $api;


    public function __construct(PokemonApi $api)
    {
        $this->api = $api;
    }

    /**
     * @Route("/pokemon/list")
     */
    public function list(): Response
    {
        $list = $this->api->list();
        return $this->render('pokemon/list.html.twig', [
            'list' => $list,
        ]);
    }

    /**
     * @Route("/pokemon/details/{index}/{strongest}", name="pokemon_details")
     * @param $index
     * @param $strongest
     * @return Response
     */
    public function details($index, $strongest): Response
    {
        $details = $this->api->details($index);
        $strongestDetails = $this->api->details($strongest);
        return $this->render('pokemon/details.html.twig', [
            'details' => $details,
            'strongestDetails' => $strongestDetails,
        ]);
    }
}