<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use DateInterval;

class PokemonApi
{
    const API_URL = 'https://pokeapi.co/api/v2/pokemon';
    const LIMIT = 100;
    const EXPIRE_SECONDS = 300;
    const POKEMON_TYPES = [
        'ground',
        'water',
    ];

    protected $httpClient;
    protected $cache;
    protected $logger;

    /**
     * PokemonApi constructor.
     * @param HttpClientInterface $httpClient
     * @param AdapterInterface $cache
     * @param LoggerInterface $logger
     */
    public function __construct(HttpClientInterface $httpClient, AdapterInterface $cache, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function list() {

        $offset = 0;
        $key = 'list_' . self::LIMIT . '_' . $offset;
        $url = self::API_URL . '?limit=' . self::LIMIT  . '&offset=' . $offset;

        $responseData = $this->cacheManager($url, $key);
        $list = [];
        foreach (self::POKEMON_TYPES as $pokemonType) {

            $strongest = null;
            $maxStrength = 0;
            $typeList = [];
            if (is_array($responseData->results)) {
                foreach ($responseData->results as $result) {

                    $parts = explode("/", rtrim($result->url, '/'));
                    $index = end($parts);
                    $details = $this->details($index);
                    if (in_array($pokemonType, $details['types'])) {
                        unset($details['types']);
                        $typeList[$index] = $details;
                        if ($details['strength'] > $maxStrength) {
                            $maxStrength = $details['strength'];
                            $strongest = $details['index'];
                        }
                    }
                }
            }
            usort($typeList,function ($item1,$item2)
            {
                if ($item1['name'] == $item2['name']) return 0;
                return ($item1['name'] > $item2['name']) ? 1 : -1;
            });
            $list[$pokemonType] = [
                'list' => $typeList,
                'strongest' => $strongest,
            ];
        }
        return $list;
    }

    /**
     * @param $index
     * @return mixed
     */
    public function details($index) {

        $key = 'details_' . $index;
        $url = self::API_URL . '/' . $index;

        $responseData = $this->cacheManager($url, $key);

        $types = [];
        if (is_array($responseData->types)) {
            foreach ($responseData->types as $typedata) {
                if (isset($typedata->type->name)) {
                    $types[] = $typedata->type->name;
                }
            }
        }
        $strength = 0;
        if (is_array($responseData->stats)) {
            foreach ($responseData->stats as $statsData) {
                if (isset($statsData->stat->name) && isset($statsData->base_stat) && in_array($statsData->stat->name, ['attack', 'defense'])) {
                    $strength += $statsData->base_stat;
                }
            }
        }
        return [
            'index' => $index,
            'types' => $types,
            'name' => $responseData->name,
            'image' => $responseData->sprites->front_default,
            'strength' => $strength,
        ];
    }

    /**
     * @param $url
     * @param $key
     * @return mixed
     */
    protected function cacheManager($url, $key) {

        try {
            $item = $this->cache->getItem($key);
            if (!$item->isHit())
            {
                $response = $this->sender('GET', $url);
                $item->set($response);
                $item->expiresAfter( new DateInterval('PT' . self::EXPIRE_SECONDS . 'S'));
                $this->cache->save($item);
            }
            $responseData = json_decode($item->get());
            if (json_last_error() != JSON_ERROR_NONE) {
                throw new \Exception('JSON decode error: ' . json_last_error());
            }

            return $responseData;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param $method
     * @param $url
     * @return string
     */
    protected function sender($method, $url) {
        $response = $this->httpClient->request(
            $method,
            $url
        );
        $responseJson = $response->getContent();
        return $responseJson;
    }
}