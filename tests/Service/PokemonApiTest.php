<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PokemonApiTest extends WebTestCase
{
    protected function setUp()
    {
        self::bootKernel();
    }

    public function testList()
    {
        $result = static::$kernel->getContainer()->get('App\Service\PokemonApi')->list();
        $this->assertIsArray($result);
    }

    public function testDetails()
    {
        $result = static::$kernel->getContainer()->get('App\Service\PokemonApi')->details(3);

        $expectedResult = [
            'index' => 3,
            'types' => [
                0 => 'grass',
                1 => 'poison'
            ],
            'name' => 'venusaur',
            'image' => 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/3.png',
            'strength' => 165
        ];

        $this->assertEquals($expectedResult, $result);
    }
}